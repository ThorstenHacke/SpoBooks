# SpoBooks
[![pipeline status](https://gitlab.com/ThorstenHacke/SpoBooks/badges/master/pipeline.svg)](https://gitlab.com/ThorstenHacke/SpoBooks/-/commits/master)

Audiobook optimized player-app for spotify on android

## Design
By Felix

### Prototype
https://xd.adobe.com/view/ce4b62a2-6047-4735-a15b-c48ffe12c81d-9b18/?fullscreen

### High Fidelity Prototype
https://xd.adobe.com/view/a860a081-3b1f-477b-83c6-1e304e648e54-fd33/?fullscreen