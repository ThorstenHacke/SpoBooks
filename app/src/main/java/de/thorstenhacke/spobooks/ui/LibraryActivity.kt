package de.thorstenhacke.spobooks.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.StoreDataRepository
import de.thorstenhacke.spobooks.data.persistence.Book
import de.thorstenhacke.spobooks.databinding.ActivityArtistBinding
import de.thorstenhacke.spobooks.databinding.ActivityLibraryBinding
import de.thorstenhacke.spobooks.databinding.ActivityOverviewBinding
import de.thorstenhacke.spobooks.databinding.RabbitHelperViewBinding
import de.thorstenhacke.spobooks.ui.playback.PlaybackActivity
import de.thorstenhacke.spobooks.ui.utilities.BookAdapter
import de.thorstenhacke.spobooks.ui.utilities.SimpleGenericViewHolder
import de.thorstenhacke.spobooks.ui.utilities.SwipeToDeleteCallback

import kotlinx.coroutines.launch


class LibraryActivity : AppCompatActivity() {


    private lateinit var rabbitBinding: RabbitHelperViewBinding
    private lateinit var overviewBinding: ActivityOverviewBinding
    private lateinit var libaryBinding: ActivityLibraryBinding;
    private val getViewHolder: (View) -> SimpleGenericViewHolder<Book> = {
        object : SimpleGenericViewHolder<Book>(it) {
            override fun getTitle(data: Book): String {
                val artistNames = data.artists
                return "${data.albumTitle} - $artistNames"
            }

            override fun getImage(data: Book): String? {
                return data.titleImageUrl
            }

            override fun getDescription(data: Book): String {
                return "Track: ${data.lastTrackNumber} von ${data.totalTrackCount}"
            }

            override fun getType(data: Book): String {
                return getString(R.string.album);
            }
            override fun handleOpenSpotify(data: Book): Boolean {
//                val intent = data.external_urls?.get("spotify")?.let { it1 -> SpotifyRepository.openSpotify(it1) }
//                startActivity(intent)
                return true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        libaryBinding = ActivityLibraryBinding.inflate(layoutInflater);
        overviewBinding = ActivityOverviewBinding.inflate(layoutInflater);
        rabbitBinding = RabbitHelperViewBinding.inflate(layoutInflater);

        setContentView(R.layout.activity_library)
        lifecycleScope.launch {
            val storeDataRepo = StoreDataRepository(applicationContext)
            val storedData = storeDataRepo.getStoreData()
            displayAlbums(storedData)

        }
        setupHelperRabbit()
        rabbitBinding.helpText.text = getString(R.string.library_help_text)
        libaryBinding.player?.Update()

    }

    private fun setupHelperRabbit() {
        //Initial move rabbit off screen
        rabbitBinding.helperRabbit.animate().translationX(800F)
        //Setup Help Button Click to show rabbit with text
        rabbitBinding.fabHelp.setOnClickListener {
            rabbitBinding.helperRabbit.visibility = View.VISIBLE
            rabbitBinding.fabHelp.visibility = View.INVISIBLE
            rabbitBinding.helperRabbit.animate().translationX(0F).setListener(null)
        }
        //Setup Rabbit click to dismiss
        rabbitBinding.helperRabbit.setOnClickListener {
            rabbitBinding.helperRabbit.animate().translationX(800F).setListener(object :
                AnimatorListenerAdapter() {
                //new fun onAnimationEnd(animation: Animator?) {
                  //  rabbitBinding.helperRabbit.visibility = View.INVISIBLE
                  //  rabbitBinding.fabHelp.visibility = View.VISIBLE
                //}
            })
        }
    }



    private fun goToPlayback(album_key: String?) {
        val intent = Intent(this, PlaybackActivity::class.java)
        intent.putExtra(getString(R.string.extra_main_play), album_key)
        startActivity(intent)
    }
    private fun displayAlbums(books: List<Book>) {
       val adapter = object: BookAdapter(libaryBinding.listViewLibrary, getViewHolder,
            books as MutableList<Book>
        ){
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): SimpleGenericViewHolder<Book> {
                val holder = super.onCreateViewHolder(parent, viewType)
                holder.itemView.setOnClickListener{
                    goToPlayback(books[libaryBinding.listViewLibrary.getChildLayoutPosition(it)].albumId)
                }
                return holder
            }
        }
        libaryBinding.listViewLibrary.adapter = adapter
        libaryBinding.listViewLibrary.layoutManager = LinearLayoutManager(this)
        val itemTouchHelper =
            ItemTouchHelper(SwipeToDeleteCallback(adapter))
        itemTouchHelper.attachToRecyclerView(libaryBinding.listViewLibrary)
        adapter.notifyDataSetChanged()
    }

}
