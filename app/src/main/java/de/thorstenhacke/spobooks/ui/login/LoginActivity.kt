package de.thorstenhacke.spobooks.ui.login

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.spotify.sdk.android.auth.AuthorizationClient
import com.spotify.sdk.android.auth.AuthorizationRequest
import com.spotify.sdk.android.auth.AuthorizationResponse
import de.thorstenhacke.spobooks.ui.LibraryActivity
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.SpotifyRepository
import de.thorstenhacke.spobooks.databinding.ActivityLoginBinding
import de.thorstenhacke.spobooks.ui.OverviewActivity


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_login)

        val login = findViewById<Button>(R.id.login)
        login.setOnClickListener {
            val redirectUri = SpotifyRepository.redirectUri
            val builder = AuthorizationRequest.Builder(
                SpotifyRepository.clientId,
                AuthorizationResponse.Type.TOKEN,
                redirectUri
            )
            builder.setScopes(arrayOf("streaming"))
            val request = builder.build()
            AuthorizationClient.openLoginActivity(this, SpotifyRepository.REQUEST_CODE, request)
        }

    }

    override fun onWindowFocusChanged(hasFocus:Boolean){
        super.onWindowFocusChanged(hasFocus)
        animateLoginContainer()
    }
    private fun animateLoginContainer(){
        val constraints = ConstraintSet()
        constraints.clone(this, R.layout.activity_login_constraints)

        TransitionManager.beginDelayedTransition(binding.loginRoot)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val autoTransition = AutoTransition()
            autoTransition.duration = 1000
            TransitionManager.beginDelayedTransition(binding.loginRoot,autoTransition)
            constraints.applyTo(binding.loginRoot)
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        intent: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, intent)

        // Check if result comes from the correct activity
        if (requestCode == SpotifyRepository.REQUEST_CODE) {
            val response =
                AuthorizationClient.getResponse(resultCode, intent)
            response.type
            when (response.type) {
                AuthorizationResponse.Type.TOKEN -> {
                    SpotifyRepository.token = response.accessToken
                    goToMainActivity()
                }
                AuthorizationResponse.Type.ERROR -> {
                }
                else -> {
                }
            }
        }
    }

    private fun goToMainActivity() {
        val myIntent = Intent(this, OverviewActivity::class.java)
        startActivity(myIntent)
    }
}
