package de.thorstenhacke.spobooks.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.SpotifyRepository
import de.thorstenhacke.spobooks.data.images.ImageRepo
import de.thorstenhacke.spobooks.data.models.Album
import de.thorstenhacke.spobooks.data.models.Artist
import de.thorstenhacke.spobooks.databinding.ActivityArtistBinding
import de.thorstenhacke.spobooks.ui.playback.PlaybackActivity
import de.thorstenhacke.spobooks.ui.utilities.SimpleGenericAdapter
import de.thorstenhacke.spobooks.ui.utilities.SimpleGenericViewHolder
import kotlinx.coroutines.launch


class ArtistActivity : AppCompatActivity() {
    private val getViewHolder: (View) -> SimpleGenericViewHolder<Album> = {
        object: SimpleGenericViewHolder<Album>(it){
            override fun getTitle(data: Album): String {
                return data.name
            }

            override fun getImage(data: Album): String? {
                return data.getTitleImageUrl()
            }

            override fun handleOpenSpotify(data: Album): Boolean {
                val intent = data.external_urls?.get("spotify")?.let { it1 -> SpotifyRepository.openSpotify(it1) }
                startActivity(intent)
                return true
            }

            override fun getType(data: Album): String {
                return getString(R.string.album);
            }
        }
    }

    private lateinit var binding: ActivityArtistBinding;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArtistBinding.inflate(layoutInflater);
        setContentView(R.layout.activity_artist)
        binding.imgArtist.setDefaultImageResId(android.R.drawable.ic_menu_gallery)
        binding.imgArtist.setErrorImageResId(android.R.drawable.ic_menu_gallery)
        val artistId = intent.getStringExtra(getString(R.string.extra_artist_id))

        lifecycleScope.launch {
            val artist = SpotifyRepository.getArtistById(baseContext, artistId)
            if (artist != null) {
                onGetArtistSuccess(artist)
            }
        }

    }

    private suspend fun onGetArtistSuccess(artist: Artist) {
        val imageUrl = artist.getTitleImageUrl()
        if(imageUrl != null) {
            binding.imgArtist.setImageUrl(imageUrl,ImageRepo.getInstance(applicationContext).imageLoader)
        }
        binding.collapsingToolbar.title = artist.name
        updateArtistsAlbumList(artist)
    }

    private suspend fun updateArtistsAlbumList(artist: Artist) {
        val list = SpotifyRepository.getAlbumsByArtist(applicationContext, artist)
        val adapter = object : SimpleGenericAdapter<Album>(binding.lvAlbums, getViewHolder,
            list as MutableList<Album>
        ) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): SimpleGenericViewHolder<Album> {
                val holder = super.onCreateViewHolder(parent, viewType)
                holder.itemView.setOnClickListener {
                    val albumId = list[binding.lvAlbums.getChildLayoutPosition(it)].id
                    goToPlayback("spotify:album:$albumId")
                }
                return holder
            }
        }
        binding.lvAlbums.adapter = adapter
        binding.lvAlbums.layoutManager = LinearLayoutManager(binding.lvAlbums.context)

        adapter.notifyDataSetChanged()
    }

    private fun goToPlayback(album_key: String) {
        val intent = Intent(this, PlaybackActivity::class.java)
        intent.putExtra(getString(R.string.extra_main_play), album_key)
        startActivity(intent)
    }
}