package de.thorstenhacke.spobooks.ui.utilities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import de.thorstenhacke.spobooks.R


abstract class SimpleGenericAdapter <T>(private val view: View, val getViewHolder: (View) -> SimpleGenericViewHolder<T>, private val values: MutableList<T>): RecyclerView.Adapter<SimpleGenericViewHolder<T>>() {


    private var recentlyDeletedItemPosition: Int?=null
    private var recentlyDeletedItem: T?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleGenericViewHolder<T> {
        val inflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.row_layout, parent, false)
        return getViewHolder(v)

    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: SimpleGenericViewHolder<T>, position: Int) {
        holder.bindItems(values[position])
    }

    fun deleteItem(position: Int) {
        recentlyDeletedItem = values[position]
        recentlyDeletedItemPosition = position
        values.removeAt(position)
        notifyItemRemoved(position)
        showUndoSnackbar()
    }

    private fun showUndoSnackbar() {
        val snackbar: Snackbar = Snackbar.make(
            this.view, R.string.library_delete_conform_question,
            Snackbar.LENGTH_LONG
        )
        snackbar.setAction(R.string.undo_text) { undoDelete() }
        snackbar.show()
    }

    private fun undoDelete() {
        if(recentlyDeletedItem != null && recentlyDeletedItemPosition != null) {
            values.add(
                recentlyDeletedItemPosition!!,
                recentlyDeletedItem!!
            )
            notifyItemInserted(recentlyDeletedItemPosition!!)
        }
    }
}