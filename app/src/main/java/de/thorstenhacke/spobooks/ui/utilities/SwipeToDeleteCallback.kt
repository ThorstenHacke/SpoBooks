package de.thorstenhacke.spobooks.ui.utilities

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import java.io.InvalidObjectException

class SwipeToDeleteCallback(private val adapter: BookAdapter) : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
    private val icon = ContextCompat.getDrawable(adapter.view.context, android.R.drawable.ic_menu_delete)
    private val background = ColorDrawable(Color.RED)
    private val backgroundCornerOffset = 20

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        adapter.deleteItem(position)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if(icon == null){
            throw InvalidObjectException("icon can not be null")
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        val itemView: View = viewHolder.itemView
        val iconPosition = Rect ()
        val bgPosition = Rect()


        if (dX != 0f) {
            val iconMargin = (itemView.height - icon.intrinsicHeight) / 2
            val offsetIncludingIcon = iconMargin+icon.intrinsicWidth

            bgPosition.top = itemView.top
            bgPosition.bottom = itemView.bottom
            iconPosition.top = itemView.top + iconMargin
            iconPosition.bottom = iconPosition.top + icon.intrinsicHeight
            if(dX > 0) {
                iconPosition.right = itemView.left + offsetIncludingIcon
                iconPosition.left = itemView.left + iconMargin
                bgPosition.left =  itemView.left
                bgPosition.right = itemView.left + dX.toInt() + backgroundCornerOffset
            }else {
                iconPosition.right = itemView.right - iconMargin
                iconPosition.left = itemView.right - offsetIncludingIcon
                bgPosition.left = itemView.right + dX.toInt() - backgroundCornerOffset
                bgPosition.right = itemView.right
            }
        }
        icon.bounds = iconPosition
        background.bounds = bgPosition

        background.draw(c)
        icon.draw(c)
    }
}