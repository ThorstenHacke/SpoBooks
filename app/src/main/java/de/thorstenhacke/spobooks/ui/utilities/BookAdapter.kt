package de.thorstenhacke.spobooks.ui.utilities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.StoreDataRepository
import de.thorstenhacke.spobooks.data.persistence.Book

abstract class BookAdapter (val view: View, val getViewHolder: (View) -> SimpleGenericViewHolder<Book>, private val values: MutableList<Book>): RecyclerView.Adapter<SimpleGenericViewHolder<Book>>() {
    private val storeDataRepo = StoreDataRepository(view.context)

    private var recentlyDeletedItemPosition: Int? = null
    private var recentlyDeletedItem: Book? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleGenericViewHolder<Book> {
        val inflater =
            parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.row_layout, parent, false)
        return getViewHolder(v)

    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: SimpleGenericViewHolder<Book>, position: Int) {
        holder.bindItems(values[position])
    }

    fun deleteItem(position: Int) {
        recentlyDeletedItem = values[position]
        recentlyDeletedItemPosition = position
        values.removeAt(position)
        storeDataRepo.deleteStoreData(recentlyDeletedItem!!)
        notifyItemRemoved(position)
        showUndoSnackbar()
    }

    private fun showUndoSnackbar() {
        val snackbar: Snackbar = Snackbar.make(
            this.view, R.string.library_delete_conform_question,
            Snackbar.LENGTH_LONG
        )
        snackbar.setAction(R.string.undo_text) { undoDelete() }
        snackbar.show()
    }

    private fun undoDelete() {
        if (recentlyDeletedItem != null && recentlyDeletedItemPosition != null) {
            values.add(
                recentlyDeletedItemPosition!!,
                recentlyDeletedItem!!
            )
            storeDataRepo.addStoreData(recentlyDeletedItem!!)
            notifyItemInserted(recentlyDeletedItemPosition!!)
        }
    }
}