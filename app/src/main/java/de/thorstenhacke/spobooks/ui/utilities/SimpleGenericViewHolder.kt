package de.thorstenhacke.spobooks.ui.utilities
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.NetworkImageView
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.images.ImageRepo

abstract class SimpleGenericViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindItems(data : T){


        val square = itemView.findViewById<NetworkImageView>(R.id.img_row_iconSquare)
        square.setDefaultImageResId(android.R.drawable.ic_menu_gallery)
        square.setErrorImageResId(android.R.drawable.ic_menu_gallery)

        val menuButton = itemView.findViewById<AppCompatImageButton>(R.id.btn_menu)
        menuButton.setOnClickListener {
            showPopup(menuButton, data)
        }

        val title = itemView.findViewById<TextView>(R.id.txt_row_title)
        val type = itemView.findViewById<TextView>(R.id.txt_row_type)
        val description = itemView.findViewById<TextView>(R.id.txt_row_desc)
        description.text = getDescription(data)
        val imageUrl = getImage(data)
        val titleText = getTitle(data)
        if(imageUrl != null) {
            square.setImageUrl(imageUrl, ImageRepo.getInstance(itemView.context).imageLoader)
        }
        title.text = titleText
        type.text = getType(data)
    }

    private fun showPopup(menuButton: AppCompatImageButton?, data: T) {
        val popup = PopupMenu(itemView.context, menuButton)
        popup.run {
            inflate(R.menu.rows_popup_menu)
            setOnMenuItemClickListener { mnuItem ->
                when (mnuItem.itemId) {
                    R.id.mnu_open_spotify -> {
                        handleOpenSpotify(data)
                    }
                    R.id.mnu_open -> {
                        itemView.callOnClick()
                    }
                }
                return@setOnMenuItemClickListener true
            }
            show()
        }
    }

    abstract fun getTitle(data: T): String

    open fun getDescription(data: T):String{
        return ""
    }
    abstract fun getType(data: T):String

    abstract fun handleOpenSpotify(data: T): Boolean

    abstract fun getImage(data: T): String?

    open fun useSquareImage(data: T): Boolean{return true}
}