package de.thorstenhacke.spobooks.ui.playback

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.spotify.protocol.client.Subscription
import com.spotify.protocol.types.Image
import com.spotify.protocol.types.PlayerState
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.SpotifyConnector
import de.thorstenhacke.spobooks.data.SpotifyRepository
import de.thorstenhacke.spobooks.data.StoreDataRepository
import de.thorstenhacke.spobooks.data.persistence.Book
import de.thorstenhacke.spobooks.databinding.ActivityPlaybackBinding
import kotlinx.coroutines.launch

class PlaybackActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlaybackBinding
    private lateinit var connector: SpotifyConnector
    private var lastKnownTrackUri: String? = null
    private lateinit var storeDataRepo: StoreDataRepository
    private var currentAlbumData: Book? = null
    private lateinit var views: List<View>
    private lateinit var trackProgressBar: TrackProgressBar
    private var openSpotifyUri:String? = null

    private val errorCallback = { throwable: Throwable -> logError(throwable) }


    private fun logError(throwable: Throwable) {
        Toast.makeText(this, "Oh no", Toast.LENGTH_SHORT).show()
        Log.e("TAG", "", throwable)
    }

    override fun onStart() {
        super.onStart()
        val toPlay = intent.getStringExtra(getString(R.string.extra_main_play))
        if (toPlay != null) {
            var data = storeDataRepo.getStoreDataById(toPlay)
            if (data == null) {
                data = Book(toPlay, 0, 0,"",0,"","")
            }
            currentAlbumData = data
        }
        binding.seekBar.apply {
            isEnabled = false
            progressDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
            indeterminateDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        }

        binding.txtOpenSpotify.setOnClickListener {
            openSpotifyUri?.let { url -> {
                val intent = SpotifyRepository.openSpotify(url)
                startActivity(intent)
            } }
        }
        trackProgressBar =
            TrackProgressBar(binding.seekBar) { seekToPosition: Long -> seekTo(seekToPosition) }

        views = listOf(
            binding.fabPlay,
            binding.fabNext,
            binding.fabPrevious,
            binding.seekBar
        )
        binding.fabPrevious.setOnClickListener { this.goToPreviousTitle() }
        binding.fabPlay.setOnClickListener { togglePlayPause() }
        binding.fabNext.setOnClickListener { goToNextTitle() }
        connector = SpotifyConnector(this, playerStateEventCallback, this::onConnected)
        connector.connect()
    }
    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlaybackBinding.inflate(layoutInflater);
        setContentView(R.layout.activity_playback)
        storeDataRepo = StoreDataRepository(applicationContext)


    }

    private fun seekTo(seekToPosition: Long) {
        connector.assertAppRemoteConnected()
            .playerApi
            .seekTo(seekToPosition)
            .setErrorCallback(errorCallback)
    }

    private val playerStateEventCallback = Subscription.EventCallback<PlayerState> { playerState ->

        updatePlayPauseButton(playerState)

        updateTrackCoverArt(playerState)

        updateSeekbar(playerState)
    }

    private fun updateSeekbar(playerState: PlayerState) {
        // Update progressbar
        trackProgressBar.apply {
            if (playerState.playbackSpeed > 0) {
                unpause()
            } else {
                pause()
            }
            // Invalidate seekbar length and position
            binding.seekBar.max = playerState.track.duration.toInt()
            binding.seekBar.isEnabled = true
            setDuration(playerState.track.duration)
            update(playerState.playbackPosition)
        }
    }

    private fun updateTrackCoverArt(playerState: PlayerState) {
        // Get image from track
        connector.assertAppRemoteConnected()
            .imagesApi
            .getImage(playerState.track.imageUri, Image.Dimension.LARGE)
            .setResultCallback { bitmap ->
                binding.imgCover.setImageBitmap(bitmap)
            }
    }

    private fun updatePlayPauseButton(playerState: PlayerState) {
        // Invalidate play / pause
        if (playerState.isPaused) {
            binding.fabPlay.setImageResource(R.drawable.ic_play_arrow_black_18dp)
        } else {
            binding.fabPlay.setImageResource(R.drawable.ic_pause_black_18dp)
        }
    }

    private fun goToNextTitle() {
        connector.assertAppRemoteConnected().playerApi.skipNext()
    }

    private fun togglePlayPause() {
        connector.assertAppRemoteConnected().let {
            it.playerApi
                .playerState
                .setResultCallback { playerState ->
                    if (playerState.isPaused) {
                        it.playerApi
                            .resume()
                            .setResultCallback {
                                //logMessage(getString(R.string.command_feedback, "play"))
                            }
                            .setErrorCallback(errorCallback)
                    } else {
                        it.playerApi
                            .pause()
                            .setResultCallback {
//                                logMessage(getString(R.string.command_feedback, "pause"))
                            }
                            .setErrorCallback(errorCallback)
                    }
                }
        }
    }

    private fun onConnected(){
        val remote = connector.assertAppRemoteConnected()
        if (currentAlbumData != null) {
            val checkedData = currentAlbumData!!
            remote.playerApi.skipToIndex(checkedData.albumId, checkedData.lastTrackNumber)
                .setResultCallback {
                    remote.playerApi.seekTo(checkedData.lastTrackPosition)
                }
        }
    }


    private fun goToPreviousTitle() {
        connector.assertAppRemoteConnected().playerApi.skipPrevious()
    }
}