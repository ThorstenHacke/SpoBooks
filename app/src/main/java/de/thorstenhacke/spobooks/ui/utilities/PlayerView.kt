package de.thorstenhacke.spobooks.ui.utilities

import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.spotify.protocol.client.Subscription
import com.spotify.protocol.types.PlayerState
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.SpotifyConnector
import de.thorstenhacke.spobooks.data.SpotifyRepository
import de.thorstenhacke.spobooks.data.StoreDataRepository
import de.thorstenhacke.spobooks.data.persistence.Book
import de.thorstenhacke.spobooks.databinding.PlayerViewBinding
import de.thorstenhacke.spobooks.ui.playback.PlaybackActivity

class PlayerView(context: Context, attrs: AttributeSet) : FrameLayout(context,attrs) {
    private lateinit var connector: SpotifyConnector
    private val errorCallback = { throwable: Throwable -> logError(throwable) }
    private val playerStateEventCallback = Subscription.EventCallback<PlayerState> { playerState ->

        updatePlayPauseButton(playerState)
        updateTrackText(playerState)
    }
    private val binding = PlayerViewBinding.inflate(LayoutInflater.from(context), this, true);



    private fun updateTrackText(playerState: PlayerState?) {
        if(playerState != null) {
            val string = DurationToMinutesSecondsString(playerState.playbackPosition)
            binding.txtPlaybackDuration.text = string;
            binding.txtPlaybackTitle.text = playerState.track.album.name
            binding.txtPlaybackTrack.text = playerState.track.name
        }
    }

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {



    }


    fun Update(){
        val storeDataRepo = StoreDataRepository(context)

        val storedData = storeDataRepo.getStoreData()

        if(storedData.count() > 0 ) {
            storedData.last().albumId
            View.inflate(context, R.layout.player_view, this)
            this.setOnClickListener { goToPlayback("") }
            binding.fabPlayPlayer.setOnClickListener { togglePlayPause() }
            connector = SpotifyConnector(context, playerStateEventCallback, this::empty)
            connector.connect()
        }else{
            this.visibility = View.GONE
        }
    }
    fun empty(){

    }
    private fun togglePlayPause() {
        connector.assertAppRemoteConnected().let {
            it.playerApi
                .playerState
                .setResultCallback { playerState ->
                    if (playerState.isPaused) {
                        it.playerApi
                            .resume()
                            .setResultCallback {
                                //logMessage(getString(R.string.command_feedback, "play"))
                            }
                            .setErrorCallback(errorCallback)
                    } else {
                        it.playerApi
                            .pause()
                            .setResultCallback {
//                                logMessage(getString(R.string.command_feedback, "pause"))
                            }
                            .setErrorCallback(errorCallback)
                    }
                }
        }
    }
    private fun goToPlayback(album_key: String?) {
        val intent = Intent(context, PlaybackActivity::class.java)
        intent.putExtra(context.getString(R.string.extra_main_play), album_key)
        context.startActivity(intent)
    }









    private fun DurationToMinutesSecondsString(duration:Long): String {
        val minutes = duration / 1000 / 60
        val seconds = (duration - (minutes * 60 * 1000)) / 1000
        val string = "$minutes:$seconds";
        return string
    }

    private fun updatePlayPauseButton(playerState: PlayerState) {
        // Invalidate play / pause
        if (playerState.isPaused) {
            binding.fabPlayPlayer.setImageResource(R.drawable.ic_play_arrow_black_18dp)
        } else {
            binding.fabPlayPlayer.setImageResource(R.drawable.ic_pause_black_18dp)
        }
    }




    private fun logError(throwable: Throwable) {
        Log.e("TAG", "", throwable)
    }
}