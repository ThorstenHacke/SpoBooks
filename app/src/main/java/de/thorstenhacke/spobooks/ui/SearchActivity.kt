package de.thorstenhacke.spobooks.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.SpotifyRepository
import de.thorstenhacke.spobooks.data.models.Item
import de.thorstenhacke.spobooks.databinding.ActivitySearchBinding
import de.thorstenhacke.spobooks.ui.playback.PlaybackActivity
import de.thorstenhacke.spobooks.ui.utilities.SimpleGenericAdapter
import de.thorstenhacke.spobooks.ui.utilities.SimpleGenericViewHolder
import kotlinx.coroutines.launch

class SearchActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySearchBinding
    private val getViewHolder: (View) -> SimpleGenericViewHolder<Item> = { it ->
        object: SimpleGenericViewHolder<Item>(it){
            override fun getTitle(data: Item): String {
                return data.name
            }

            override fun getImage(data: Item): String? {
                return data.getTitleImageUrl()
            }

            override fun useSquareImage(data: Item): Boolean {
                return data.type != getString(R.string.item_type_artist)
            }

            override fun getType(data: Item): String {
                return if (data.type != getString(R.string.item_type_artist)) {
                    getString(R.string.album)
                } else {
                    getString(R.string.artist)
                }
            }
            override fun handleOpenSpotify(data: Item): Boolean {
                val intent = data.external_urls?.get("spotify")?.let { it1 -> SpotifyRepository.openSpotify(it1) }
                startActivity(intent)
                return true
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater);
        setContentView(R.layout.activity_search)
        binding.listViewSearch.layoutManager = LinearLayoutManager(this)
        binding.searchView.setOnQueryTextListener(
            object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    lifecycleScope.launch {
                        binding.infoTextContainer.visibility = View.INVISIBLE
                        binding.searchProgress.visibility = View.VISIBLE
                        if (query != null) {
                            queryArtists(query)
                        }
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            }
        )
        binding.searchView.requestFocus()

    }

    private suspend fun queryArtists(query : String){
        val spotifyRepo = SpotifyRepository
        binding.searchView.clearFocus()
        val items = spotifyRepo.getItemsByQuery(baseContext,query)
        displayArtists(items)
    }

    private fun displayArtists(items: List<Item>) {
        val adapter = object : SimpleGenericAdapter<Item>(binding.listViewSearch, getViewHolder,
            items as MutableList<Item>
        ){
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): SimpleGenericViewHolder<Item> {
                val holder = super.onCreateViewHolder(parent, viewType)
                holder.itemView.setOnClickListener{ view ->
                    handleSearchItemClick(items, view)
                }
                return holder
            }
        }
        binding.listViewSearch.adapter = adapter
        adapter.notifyDataSetChanged()
        binding.searchProgress.visibility = View.INVISIBLE
    }

    private fun handleSearchItemClick(
        items: List<Item>,
        view: View
    ) {
        val item = items[binding.listViewSearch.getChildLayoutPosition(view)]
        val context = view.context
        if (item.type == getString(R.string.item_type_artist)) {
            val intent = Intent(context, ArtistActivity::class.java)
            intent.putExtra(getString(R.string.extra_artist_id), item.id)
            startActivity(intent)
        } else if (item.type == getString(R.string.item_type_album)) {
            val intent = Intent(context, PlaybackActivity::class.java)
            intent.putExtra(getString(R.string.extra_main_play), "spotify:album:${item.id}")
            startActivity(intent)
        }
    }
}
