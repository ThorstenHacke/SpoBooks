package de.thorstenhacke.spobooks.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.lifecycleScope
import de.thorstenhacke.spobooks.R
import de.thorstenhacke.spobooks.data.StoreDataRepository
import de.thorstenhacke.spobooks.databinding.ActivityLibraryBinding
import de.thorstenhacke.spobooks.databinding.ActivityOverviewBinding
import de.thorstenhacke.spobooks.databinding.RabbitHelperViewBinding

import kotlinx.coroutines.launch

class OverviewActivity : AppCompatActivity() {
    private lateinit var overviewBinding: ActivityOverviewBinding
    override fun onStart() {
        super.onStart()




        lifecycleScope.launch {
            val storeDataRepo = StoreDataRepository(applicationContext)
            val storedData = storeDataRepo.getStoreData()
            if(storedData.count() > 0 ) {
                overviewBinding.fabLibrary.visibility = View.VISIBLE
                overviewBinding.fabOverviewSearch.clearAnimation()
            }else{
                overviewBinding.fabLibrary.visibility = View.GONE
                val animation1: Animation =
                    AnimationUtils.loadAnimation(applicationContext,
                        R.anim.fab_pulse
                    )
                overviewBinding.fabOverviewSearch.startAnimation(animation1)
            }
        }
        overviewBinding.overviewPlayer.Update()
        overviewBinding.fabLibrary.setOnClickListener{
            goToLibrary()
        }
        overviewBinding.fabOverviewSearch.setOnClickListener {
            goToSearch()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overviewBinding = ActivityOverviewBinding.inflate(layoutInflater);
        setContentView(R.layout.activity_overview)

        val animation1: Animation =
            AnimationUtils.loadAnimation(applicationContext,
                R.anim.fab_pulse
            )
        overviewBinding.fabOverviewSearch.startAnimation(animation1)

    }
    private fun goToSearch() {
        val intent = Intent(this, SearchActivity::class.java)
        startActivity(intent)
    }

    private fun goToLibrary() {
        val intent = Intent(this, LibraryActivity::class.java)
        startActivity(intent)
    }
}