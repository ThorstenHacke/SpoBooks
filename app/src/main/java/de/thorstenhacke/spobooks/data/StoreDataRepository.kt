package de.thorstenhacke.spobooks.data

import android.content.Context
import androidx.room.Room
import de.thorstenhacke.spobooks.data.persistence.AppDatabase
import de.thorstenhacke.spobooks.data.persistence.Book

class StoreDataRepository(context: Context) {

    private var db: AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "spobooks-data"
    ).allowMainThreadQueries().build()


    fun getStoreData(): List<Book> {
        return db.bookDao().getAll()
    }
    fun getStoreDataById(id: String): Book?{
        return db.bookDao().loadById(id)
    }

    fun addStoreData(data: Book) {
        db.bookDao().insertAll(data)
    }

    fun deleteStoreData(data: Book){
        db.bookDao().delete(data)
    }

    fun updateStoreData(data: Book){
        if(db.bookDao().loadById(data.albumId) == null){
            db.bookDao().insertAll(data)
        }else {
            db.bookDao().updateAll(data)
        }
    }
}