package de.thorstenhacke.spobooks.data

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.beust.klaxon.Klaxon
import com.spotify.android.appremote.api.ConnectionParams
import de.thorstenhacke.spobooks.data.models.*
import de.thorstenhacke.spobooks.data.search.SpotifySearchBase


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

object SpotifyRepository {

    var token: String? = null
    var clientId = "ed4fbc386a034563a5705cd63693f6a6"
    var redirectUri = "spobooks://callback"
    var REQUEST_CODE = 1337

    suspend fun getAlbumsByArtist(context: Context, artist: Artist) :List<Album>{
        val albumsOfArtistQuery = "artists/${artist.id}/albums"
        val response = SpotifyAPIService(context).callAPI(albumsOfArtistQuery)
        val albums = Klaxon().parse<Albums>(response)
        return albums?.items ?: listOf()
    }

    suspend fun getArtistById(context: Context, artistId: String?) : Artist?{
        val response = SpotifyAPIService(context).callAPI("artists/$artistId")
        return Klaxon().parse<Artist>(response)
    }

    suspend fun getTrack(context: Context, id: String?): Track? {
        val response = SpotifyAPIService(context).callAPI("tracks/$id")
        return Klaxon().parse<Track>(response)
    }

    suspend fun getItemsByQuery(context: Context, query: String): List<Item> {
        val artistSearch: SpotifySearchBase<Item> = object : SpotifySearchBase<Item>(listOf("artist","album")) {
            override fun extractFromResponse(
                responses: List<SearchResponse?>,
                searchText: String
            ): List<Item> {

                val items = mutableListOf<Item>()
                responses.forEach { response ->
                    if(response != null) {
                        if (response.artists != null) {
                            items.addAll(response.artists.items.filter {
                                it.name.contains(
                                    searchText,
                                    ignoreCase = true
                                )
                            })
                        }
                        if (response.albums != null) {
                            items.addAll(response.albums.items.filter {
                                it.name.contains(
                                    searchText,
                                    ignoreCase = true
                                )
                            })
                        }
                    }
                }
                return items.sortedBy { i ->i.name }
            }
        }
        return artistSearch.search(query, context)
    }

    val connectionParameter: ConnectionParams?
        get() {
            return ConnectionParams.Builder(clientId)
                .setRedirectUri(redirectUri)
                .showAuthView(true)
                .build()
        }

    fun openSpotify(uri:String): Intent {
        return Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    }
}
