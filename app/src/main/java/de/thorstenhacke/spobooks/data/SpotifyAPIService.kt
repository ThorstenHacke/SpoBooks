package de.thorstenhacke.spobooks.data

import android.content.Context
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class SpotifyAPIService(val context: Context) {
    private val baseUrl = "https://api.spotify.com/v1/"
    private val queue = Volley.newRequestQueue(context)

    suspend fun callAPI(urlPath: String) =
        suspendCoroutine { continuation: Continuation<String> ->
        val url = "$baseUrl$urlPath"
        val token = "Bearer ${SpotifyRepository.token}"
        val stringRequest = object : StringRequest(
            Method.GET, url,
            Response.Listener { response ->
                continuation.resume(response)
            },
            Response.ErrorListener { error ->
                continuation.resumeWithException(error)
            }
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = token
                return headers
            }
        }
        queue.add(stringRequest)
    }
}