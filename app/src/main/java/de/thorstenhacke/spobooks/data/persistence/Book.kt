package de.thorstenhacke.spobooks.data.persistence

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Book(
    @PrimaryKey @ColumnInfo(name = "album_id") val albumId: String,
    @ColumnInfo(name = "last_track_number") var lastTrackNumber: Int,
    @ColumnInfo(name = "last_track_position") var lastTrackPosition: Long,
    @ColumnInfo(name = "title_image_url") var titleImageUrl: String,
    @ColumnInfo(name = "total_track_count") var totalTrackCount: Int,
    @ColumnInfo(name = "album_title") var albumTitle: String,
    @ColumnInfo(name = "artists") var artists: String
)