package de.thorstenhacke.spobooks.data.persistence

import androidx.room.*

@Dao
interface BookDao {


        @Query("SELECT * FROM book")
        fun getAll(): List<Book>

        @Query("SELECT * FROM book WHERE album_id IN (:albumIds)")
        fun loadAllByIds(albumIds: List<String>): List<Book>

        @Query("SELECT * FROM book WHERE album_id LIKE (:albumId)")
        fun loadById(albumId: String): Book?

        @Insert
        fun insertAll(vararg book: Book)

        @Update
        fun updateAll(vararg book: Book)
        @Delete
        fun delete(book: Book)

}