package de.thorstenhacke.spobooks.data.models
data class SearchResponse(
    val albums: Albums?=null,
    val artists: Artists?=null,
    val episodes: Episodes?=null,
    val shows: Shows?=null,
    val tracks: Tracks?=null
)

data class Albums(
    val href: String?,
    val items: List<Album>,
    val limit: Int?=null,
    val next: String?=null,
    val offset: Int?=null,
    val previous: String?=null,
    val total: Int?=null
)
data class AlbumList(
    val albums: List<Album>
)

data class Artists(
    val href: String,
    val items: List<Artist>,
    val limit: Int,
    val next: String?=null,
    val offset: Int,
    val previous: String?=null,
    val total: Int
)

data class Episodes(
    val href: String,
    val items: List<Episode>,
    val limit: Int,
    val next: String,
    val offset: Int,
    val previous: Any,
    val total: Int
)

data class Episode(
    val audio_preview_url: String,
    val description: String,
    val duration_ms: Int,
    val explicit: Boolean,
    val external_urls: MutableMap<String,String>,
    val href: String,
    val id: String,
    val images: List<Image>,
    val is_externally_hosted: Boolean,
    val is_playable: Boolean,
    val language: String,
    val languages: List<String>,
    val name: String,
    val release_date: String,
    val release_date_precision: String,
    val resume_point: ResumePoint,
    val type: String,
    val uri: String
)
data class Shows(
    val href: String,
    val items: List<Show>,
    val limit: Int,
    val next: String,
    val offset: Int,
    val previous: Any,
    val total: Int
)

data class Tracks(
    val href: String,
    val items: List<Track>,
    val limit: Int,
    val next: String,
    val offset: Int,
    val previous: Any,
    val total: Int
)



data class Image(
    val height: Int,
    val url: String,
    val width: Int
)



data class Followers(
    val href: String?=null,
    val total: Int
)






data class ResumePoint(
    val fully_played: Boolean,
    val resume_position_ms: Int
)

data class Show(
    val available_markets: List<String>,
    val copyrights: List<Any>,
    val description: String,
    val explicit: Boolean,
    val external_urls: MutableMap<String,String>,
    val href: String,
    val id: String,
    val images: List<Image>,
    val is_externally_hosted: Boolean,
    val languages: List<String>,
    val media_type: String,
    val name: String,
    val publisher: String,
    val type: String,
    val uri: String
)





data class Track(
    val album: Album,
    val artists: List<Artist>,
    val available_markets: List<String>,
    val disc_number: Int,
    val duration_ms: Int,
    val explicit: Boolean,
    val external_ids: ExternalIds,
    val external_urls: MutableMap<String,String>,
    val href: String,
    val id: String,
    val is_local: Boolean,
    val name: String,
    val popularity: Int,
    val preview_url: Any,
    val track_number: Int,
    val type: String,
    val uri: String
)
abstract class Item(
    val name: String,
    val id: String,
    val href: String?,
    val external_urls: MutableMap<String,String>?,
    val images: List<Image>?,
    val type: String?,
    val uri: String?
){
    fun getTitleImageUrl():String?{
        if(images == null || images.isEmpty()){
            return null
        }
        return images.first().url
    }
}
@Suppress("unused")
class Album (
    val album_group: String?=null,
    val album_type: String?,
    val artists: List<Artist>?,
    val available_markets: List<String>?=null,
    external_urls: MutableMap<String,String>,
    href: String?,
    id: String,
    images: List<Image>,
    name: String,
    val release_date: String,
    val release_date_precision: String,
    val total_tracks: Int?,
    type: String?,
    uri: String?,
    val genres: List<String>?=null
) : Item(name,id,href,external_urls,images,type,uri)
@Suppress("unused")
class Artist (
    external_urls: MutableMap<String,String>? =null,
    val followers: Followers?=null,
    href: String,
    id: String,
    name: String,
    type: String,
    uri: String,
    images: List<Image>?=null
): Item(name,id,href,external_urls,images,type,uri)


data class ExternalIds(
    val isrc: String
)

