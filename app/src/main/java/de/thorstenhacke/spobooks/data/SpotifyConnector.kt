package de.thorstenhacke.spobooks.data

import android.content.Context
import com.spotify.android.appremote.api.Connector
import com.spotify.android.appremote.api.SpotifyAppRemote
import com.spotify.android.appremote.api.error.SpotifyDisconnectedException
import com.spotify.protocol.client.Subscription
import com.spotify.protocol.types.PlayerState
import de.thorstenhacke.spobooks.data.models.Track
import de.thorstenhacke.spobooks.data.persistence.Book
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class SpotifyConnector(
    val context: Context,
    private val playerStateEventCallback: Subscription.EventCallback<PlayerState>,
    private val onConnectedCallback: () -> Unit

    ) {
    private val storeDataRepo = StoreDataRepository(context)
    private var lastKnownTrackUri: String? = null
    private var spotifyAppRemote: SpotifyAppRemote? = null
    private var playerStateSubscription: Subscription<PlayerState>? = null

    fun connect() {

        val coroutineScope = CoroutineScope(Dispatchers.Main)
        coroutineScope.launch {
            try {
                spotifyAppRemote = connectToAppRemote()
                onConnected()
            } catch (error: Throwable) {
            }
        }
    }

    private fun updateLastTrackInfo(playerState: PlayerState?) {
        if (playerState == null) return
        val coroutineScope = CoroutineScope(Dispatchers.Main)
        coroutineScope.launch {
            saveTrackData(playerState)
        }
    }

    private suspend fun saveTrackData(playerState: PlayerState) {
        if (playerState.track == null) return
        val albumId = playerState.track.album.uri ?: return
        val book = getBookFromDataStore(albumId) ?: return
        if (playerState.track.uri != lastKnownTrackUri) {
            lastKnownTrackUri = playerState.track.uri

            val track = SpotifyRepository.getTrack(
                context,
                playerState.track.uri.replace("spotify:track:", "")
            )
            if (track != null) {
                updateDataStoreBookWithTrackInfo(book, track)
            }

        }
        book.lastTrackPosition = playerState.playbackPosition
        storeDataRepo.updateStoreData(book)
    }

    private fun updateDataStoreBookWithTrackInfo(book: Book, track: Track) {
        book.lastTrackNumber = track.track_number - 1
        book.artists = track.artists.joinToString(
            ",",
            transform = { artist -> artist.name })
        book.totalTrackCount = track.album.total_tracks!!
        book.albumTitle = track.album.name
        book.titleImageUrl = track.album.getTitleImageUrl()!!
    }

    private fun getBookFromDataStore(albumId: String): Book? {
        var current = storeDataRepo.getStoreDataById(albumId)
        if (current == null) {
            current = Book(albumId, 0, 0, "", 0, "", "")
        }
        return current
    }

    private fun onConnected() {
        playerStateSubscription = cancelAndResetSubscription(playerStateSubscription)
        playerStateSubscription = assertAppRemoteConnected()
            .playerApi
            .subscribeToPlayerState()
            .setEventCallback {
                playerState ->
                    updateLastTrackInfo(playerState)
                    playerStateEventCallback.onEvent(playerState)
            }
            .setLifecycleCallback(
                object : Subscription.LifecycleCallback {
                    override fun onStart() {
                    }

                    override fun onStop() {
                    }
                })
            .setErrorCallback {
            } as Subscription<PlayerState>
        onConnectedCallback()


    }

    fun assertAppRemoteConnected(): SpotifyAppRemote {
        spotifyAppRemote?.let {
            if (it.isConnected) {
                return it
            }
        }
        throw SpotifyDisconnectedException()
    }
    private fun <T : Any?> cancelAndResetSubscription(subscription: Subscription<T>?): Subscription<T>? {
        return subscription?.let {
            if (!it.isCanceled) {
                it.cancel()
            }
            null
        }
    }
    private suspend fun connectToAppRemote(): SpotifyAppRemote? =
        suspendCoroutine { cont: Continuation<SpotifyAppRemote> ->
            SpotifyAppRemote.connect(
                context, SpotifyRepository.connectionParameter,
                object : Connector.ConnectionListener {
                    override fun onConnected(spotifyAppRemote: SpotifyAppRemote) {
                        cont.resume(spotifyAppRemote)
                    }

                    override fun onFailure(error: Throwable) {
                        cont.resumeWithException(error)
                    }
                })
        }
}