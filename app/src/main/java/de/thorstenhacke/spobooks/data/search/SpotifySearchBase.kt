package de.thorstenhacke.spobooks.data.search

import android.content.Context
import com.beust.klaxon.Klaxon
import de.thorstenhacke.spobooks.data.SpotifyAPIService
import de.thorstenhacke.spobooks.data.models.SearchResponse
import java.net.URLEncoder

abstract class SpotifySearchBase<T>(private val types:List<String>) {

    suspend fun search(searchText: String, context: Context) : List<T> {
        val searchResponses = mutableListOf<SearchResponse?>()
        types.forEach { type ->
            val searchTextVal = URLEncoder.encode(searchText, "UTF-8").replace("+", "%20")
            val response = SpotifyAPIService(context).callAPI(
                "search?q=$type:\"$searchTextVal\"&type=$type&market=DE"
            )
            val searchResponse = Klaxon().parse<SearchResponse>(response)
            searchResponses.add(searchResponse)
        }
        return extractFromResponse(searchResponses, searchText)
    }

    abstract fun extractFromResponse(responses: List<SearchResponse?>, searchText:String) : List<T>
}