package de.thorstenhacke.spobooks.data.images

import android.content.Context
import android.graphics.Bitmap
import android.util.LruCache
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley

class ImageRepo(context: Context) {

   companion object {
       private var instance: ImageRepo?=null

       fun getInstance(context: Context): ImageRepo {
           if (instance == null) {
               instance = ImageRepo(context)
           }
           return instance!!
       }
   }

    private var requestQueue: RequestQueue = Volley.newRequestQueue(context)
    val imageLoader: ImageLoader =  ImageLoader(requestQueue, object : ImageLoader.ImageCache {
        private val mCache: LruCache<String, Bitmap> =
            LruCache(10)

        override fun putBitmap(url: String, bitmap: Bitmap) {
            mCache.put(url, bitmap)
        }

        override fun getBitmap(url: String): Bitmap? {
            return mCache.get(url)
        }
    })
}