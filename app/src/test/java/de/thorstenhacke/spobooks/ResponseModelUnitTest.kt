package de.thorstenhacke.spobooks

import com.beust.klaxon.Klaxon
import de.thorstenhacke.spobooks.data.models.*
import org.junit.Assert
import org.junit.Test
import java.io.File

class ResponseModelUnitTest {
    @Test
    fun deserializeArtistTest() {
        val testData = "{\n" +
                "            \"external_urls\": {\n" +
                "              \"spotify\": \"https://open.spotify.com/artist/13bDjug9N0pyv3ZUINjkDV\"\n" +
                "            },\n" +
                "            \"href\": \"https://api.spotify.com/v1/artists/13bDjug9N0pyv3ZUINjkDV\",\n" +
                "            \"id\": \"13bDjug9N0pyv3ZUINjkDV\",\n" +
                "            \"name\": \"Bausa\",\n" +
                "            \"type\": \"artist\",\n" +
                "            \"uri\": \"spotify:artist:13bDjug9N0pyv3ZUINjkDV\"\n" +
                "          }"
        val artist = Klaxon()
            .parse<Artist>(testData)
        Assert.assertNotNull(artist)
        Assert.assertEquals("Bausa", artist!!.name)
    }

    @Test
    fun deserializeAlbumTest(){
        val album = Klaxon()
            .parse<Album>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testAlbumResponse.json"))
        Assert.assertNotNull(album)
        Assert.assertEquals("Dreifarbenhaus", album!!.name)
    }

    @Test
    fun deserializeAlbumsTest(){
        val albums = Klaxon()
            .parse<Albums>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testAlbumsResponse.json"))
        Assert.assertNotNull(albums)
        Assert.assertEquals(74, albums!!.total)
    }

    @Test
    fun deserializeAlbumsByIdsTest(){
        val albums = Klaxon()
            .parse<AlbumList>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testAlbumsByIdsResponse.json"))
        Assert.assertNotNull(albums)
        Assert.assertEquals(4, albums!!.albums.count())
        Assert.assertNull(albums.albums[3])
    }

    @Test
    fun deserializeAlbumFromIdsQueryTest(){
        val albums = Klaxon()
            .parse<Album>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testAlbumFromIdsQueryData.json"))
        Assert.assertNotNull(albums)
    }
    @Test
    fun deserializeFakeAlbumTest(){
        val albums = Klaxon()
            .parse<Album>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testFakeAlbum.json"))
        Assert.assertNotNull(albums)
    }
    @Test
    fun deserializeArtistTest2(){
        val artist= Klaxon()
        .parse<Artist>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testArtistResponse.json"))
        Assert.assertNotNull(artist)
        Assert.assertEquals("artist", artist!!.type)
    }
    @Test
    fun deserializeBigResultTest(){
        val searchResponse = Klaxon()
            .parse<SearchResponse>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testSearchResponse.json"))
        Assert.assertNotNull(searchResponse)
        Assert.assertEquals(10020, searchResponse!!.albums!!.total)
    }
    @Test
    fun mixedSearchResultTest(){
        val searchResponse = Klaxon()
            .parse<SearchResponse>(File("./src/test/java/de/thorstenhacke/spobooks/testdata/testSearchAlbumArtistMix.json"))
        Assert.assertNotNull(searchResponse)
        Assert.assertEquals(2, searchResponse!!.albums!!.items.size)
        Assert.assertEquals(2, searchResponse.artists!!.items.size)

    }
}